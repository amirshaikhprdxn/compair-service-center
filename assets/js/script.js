// hamburger functionality starts
$('.hamburger').click(function(e) {
	e.preventDefault();
	$this = $(this);
	$this.siblings('ul').toggleClass('block');
	$this.children('.line1').toggleClass('ham1');
	$this.children('.line3').toggleClass('ham3');
	$this.children('.line2').toggleClass('ham2');
});

// modal functionality starts
var modal_content;
$modal = $('.modal');
$modal_container = $('.modal-container');
$html = $('html');
// display image modal
$('.open-image').click(function(){
	$html.addClass('remove-scroll');
	$modal.addClass('show-modal');
	modal_content = $("<img></img>");
	modal_content.attr("src", $(this).attr('src'));
	$modal_container.append(modal_content);
});
// modal closed when clicked on cross
$('.close').click(function(){
	$html.removeClass('remove-scroll');
	$modal.removeClass('show-modal');
	$modal_container.find('img').remove();
});
// avoid bubbling i.e prevent modal from closing when clicked on modal container
$modal_container.click(function( event ) {
  	event.stopPropagation();
});
// modal closed when clicked on modal
$modal.click(function(e){
	if ($(this).hasClass('show-modal')) {
		$html.removeClass('remove-scroll');
		$modal.removeClass('show-modal');
		$modal_container.find('img').remove();
	}
});

// back to top functionality starts
$(window).scroll(function() {
	var window_top = $(window).scrollTop();
	$('.toggle-top').addClass('hide');
	if (window_top != 0) {
		$('.toggle-top').removeClass('hide');
	}
});
$('.toggle-top').click(function(e) {
	e.preventDefault();
	$("html, body").animate({scrollTop: 0}, 100);
});